#include "LostState.hpp"

LostState* LostState::GetInstance(Game* game, GameState* playingState)
{
	static LostState* _instance = new LostState(game, playingState);
	return _instance;
}

LostState::LostState(Game* game, GameState* playingState)
	:GameState(game)
	, m_playingState(static_cast<PlayingState*>(playingState))
	, _countDown(sf::Time::Zero)
{
	_text.setFont(game->getFont());
	_text.setString("YOU LOST");
	_text.setCharacterSize(30);
	_text.setPosition(100, 120);

	_countDownText.setFont(game->getFont());
	_countDownText.setCharacterSize(15);
	_countDownText.setPosition(100, 240);
}

void LostState::insertCoin()
{
	m_playingState->resetCurrentLevel();
	m_playingState->resetLiveCount();

	getGame()->changeGameState(GameState::GetReady);
}

void LostState::pressButton()
{
}

void LostState::moveStick(sf::Vector2i direction)
{
}

void LostState::update(sf::Time delta)
{
	_countDown += delta;

	if (_countDown.asSeconds() >= 10) {
		_countDown = sf::Time::Zero;

		m_playingState->resetToZero();
		getGame()->changeGameState(GameState::NoCoin);
	}

	_countDownText.setString("Insert a coin to continue..." +  std::to_string(10 - static_cast<int>(_countDown.asSeconds())));
}

void LostState::draw(sf::RenderWindow& window)
{
	window.draw(_text);
	window.draw(_countDownText);
}
