#ifndef PACMAN_LOSTSTATE_HPP
#define PACMAN_LOSTSTATE_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameState.hpp"
#include "PlayingState.hpp"
#include "Game.hpp"

class PlayingState;

class LostState: public GameState {

	public:

		LostState(Game* game, GameState* playingState);
		static LostState* GetInstance(Game* game, GameState* playingState);
		void insertCoin();
		void pressButton();
		void moveStick(sf::Vector2i direction);
		void update(sf::Time delta);
		void draw(sf::RenderWindow& window);

	private:
		sf::Text _text;
		sf::Text _countDownText;
		sf::Time _countDown;

		PlayingState* m_playingState;
};

#endif //PACMAN_WONSTATE_HPP