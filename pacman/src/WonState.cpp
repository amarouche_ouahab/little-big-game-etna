#include "WonState.hpp"

WonState* WonState::GetInstance(Game* game, GameState* playingState)
{
	static WonState* _instance = new WonState(game, playingState);
	return _instance;
}

WonState::WonState(Game* game, GameState* playingState)
	:GameState(game)
	, m_playingState(static_cast<PlayingState*>(playingState))
{
	_text.setFont(game->getFont());
	_text.setString("You Won!");
	_text.setCharacterSize(30);
	_text.setPosition(120, 240);
}

void WonState::insertCoin()
{
}

void WonState::pressButton()
{
}

void WonState::moveStick(sf::Vector2i direction)
{
}

void WonState::update(sf::Time delta)
{
	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	if (timeBuffer.asSeconds() > 3) {
		m_playingState->loadNextLevel();

		getGame()->changeGameState(GameState::GetReady);
	}
}

void WonState::draw(sf::RenderWindow& window)
{
	window.draw(_text);
}
