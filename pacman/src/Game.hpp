#ifndef PACMAN_GAME_HPP
#define PACMAN_GAME_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include <array>

#include "GameState.hpp"
#include "NoCoinState.hpp"
#include "GetReadyState.hpp"
#include "PlayingState.hpp"
#include "WonState.hpp"
#include "LostState.hpp"


class Game {
public:
	Game();
	~Game();
	void run();
	void changeGameState(GameState::State state);
	sf::Font& getFont();
	sf::Texture& getLogo();
	sf::Texture& getTexture();

private:
	sf::RenderWindow window;
	GameState* currentState;
	std::array<GameState*, GameState::Count> gameStates;

	sf::Font font;
	sf::Texture logo;
	sf::Texture texture;
};

#endif //PACMAN_GAME_HPP