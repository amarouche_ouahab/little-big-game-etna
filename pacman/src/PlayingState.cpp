#include "PlayingState.hpp"

PlayingState* PlayingState::GetInstance(Game* game)
{
	static PlayingState* _instance = new PlayingState(game);
	return _instance;
}

PlayingState::PlayingState(Game* game)
	:GameState(game)
	, _maze(game->getTexture())
	, _pacMan(nullptr)
	, m_level(0)
	, m_liveCount(3)
	, m_score(0)
{
	_pacMan = new PacMan(game->getTexture());
	_pacMan->setMaze(&_maze);
	_pacMan->setPosition(_maze.mapCellToPixel(_maze.getPacManPosition()));

	resetToZero();

	_camera.setSize(sf::Vector2f(480, 480));
	m_scene.create(480, 480);

	m_scoreText.setFont(game->getFont());
	m_scoreText.setCharacterSize(10);
	m_scoreText.setPosition(10, 480);

	m_levelText.setFont(game->getFont());
	m_levelText.setCharacterSize(10);
	m_levelText.setPosition(160, 480);

	m_remainingDotsText.setFont(game->getFont());
	m_remainingDotsText.setCharacterSize(10);
	m_remainingDotsText.setPosition(280, 480);

	for (auto& liveSprite : m_liveSprite)
	{
		liveSprite.setTexture(game->getTexture());
		liveSprite.setTextureRect(sf::IntRect(122, 0, 20, 20));
	}

	m_liveSprite[0].setPosition(sf::Vector2f(415, 480));
	m_liveSprite[1].setPosition(sf::Vector2f(435, 480));
	m_liveSprite[2].setPosition(sf::Vector2f(455, 480));
}

PlayingState::~PlayingState()
{
	delete _pacMan;

	for (Ghost* ghost : m_ghosts)
		delete ghost;
}

void PlayingState::insertCoin()
{
}

void PlayingState::pressButton()
{
}

void PlayingState::moveStick(sf::Vector2i direction)
{
	_pacMan->setDirection(direction);
}

void PlayingState::loadNextLevel()
{
	m_level++;

	int mapLevel = m_level % 3;
	int speedLevel = std::floor(m_level / 3);

	if (mapLevel == 0)
		_maze.loadLevel("small");
	else if (mapLevel == 1)
		_maze.loadLevel("medium");
	else if (mapLevel == 2)
		_maze.loadLevel("large");

	// Destroy previous characters
	for (Ghost* ghost : m_ghosts)
		delete ghost;

	m_ghosts.clear();

	// Create new characters
	for (auto ghostPosition : _maze.getGhostPositions())
	{
		Ghost* ghost = new Ghost(getGame()->getTexture(), _pacMan);
		ghost->setMaze(&_maze);
		//ghost->setPosition(_maze.mapCellToPixel(ghostPosition));

		m_ghosts.push_back(ghost);
	}

	// Change speed according to the new level
	float speed = 100 + (mapLevel * 50) + (speedLevel * 50);

	_pacMan->setSpeed(speed + 25);

	for (auto& ghost : m_ghosts)
		ghost->setSpeed(speed);

	moveCharactersToInitialPosition();

	//Update level text
	m_levelText.setString("level " + std::to_string(speedLevel) + " - " + std::to_string(mapLevel + 1));

}

void PlayingState::resetToZero()
{
	resetLiveCount();

	m_level = 0;
	resetCurrentLevel();

	m_score = 0;
}

void PlayingState::resetCurrentLevel()
{
	m_level--;
	loadNextLevel();
}

void PlayingState::resetLiveCount()
{
	_pacMan->setLife(3);
	m_liveCount = 3;
}

void PlayingState::moveCharactersToInitialPosition()
{
	_pacMan->setPosition(_maze.mapCellToPixel(_maze.getPacManPosition()));

	auto ghostPositions = _maze.getGhostPositions();
	for (unsigned int i = 0; i < m_ghosts.size(); i++)
		m_ghosts[i]->setPosition(_maze.mapCellToPixel(ghostPositions[i]));

	updateCameraPosition();
}

void PlayingState::updateCameraPosition()
{
	_camera.setCenter(_pacMan->getPosition());

	if (_camera.getCenter().x < 240)
		_camera.setCenter(240, _camera.getCenter().y);
	if (_camera.getCenter().y < 240)
		_camera.setCenter(_camera.getCenter().x, 240);

	if (_maze.getSize().x * 32 <= _camera.getCenter().x + 240)
		_camera.setCenter(_maze.getSize().x * 32 - 240, _camera.getCenter().y);
	if (_maze.getSize().y * 32 <= _camera.getCenter().y + 240)
		_camera.setCenter(_camera.getCenter().x, _maze.getSize().y * 32 - 240);

}

void PlayingState::update(sf::Time delta)
{
	_pacMan->update(delta);

	for (Ghost* ghost : m_ghosts)
		ghost->update(delta);

	sf::Vector2f pixelPosition = _pacMan->getPosition();
	sf::Vector2f offset(std::fmod(pixelPosition.x, 32), std::fmod(pixelPosition.y, 32));
	offset -= sf::Vector2f(16, 16);

	if (offset.x <= 2 && offset.x >= -2 && offset.y <= 2 && offset.y >= -2)
	{
		sf::Vector2i cellPosition = _maze.mapPixelToCell(pixelPosition);

		if (_maze.isDot(cellPosition))
		{
			m_score += 5;
		}
		else if (_maze.isSuperDot(cellPosition))
		{
			for (Ghost* ghost : m_ghosts)
				ghost->setWeak(sf::seconds(5));

			m_score += 25;
		}
		else if (_maze.isBonus(cellPosition))
		{
			m_score += 500;
		}

		_maze.pickObject(cellPosition);
	}

	for (Ghost* ghost : m_ghosts)
	{
		if (ghost->getCollisionBox().intersects(_pacMan->getCollisionBox()))
		{
			if (ghost->isWeak())
			{
				m_ghosts.erase(std::find(m_ghosts.begin(), m_ghosts.end(), ghost));

				m_score += 100;
			}
			else
				_pacMan->die();
		}
	}

	if (_pacMan->isDead())
	{
		_pacMan->reset();
		_pacMan->lostLife();

		if (_pacMan->getLife() < 0)
			getGame()->changeGameState(GameState::Lost);
		else
			moveCharactersToInitialPosition();
	}


	if (_maze.getRemainingDots() == 0)
	{
		getGame()->changeGameState(GameState::Won);
	}

	updateCameraPosition();

	// Update score text and remaining dots
	m_scoreText.setString(std::to_string(m_score) + " points");
	m_remainingDotsText.setString(std::to_string(_maze.getRemainingDots()) + "x dots");
}

void PlayingState::draw(sf::RenderWindow& window)
{
	m_scene.clear();
	m_scene.draw(_maze);
	m_scene.draw(*_pacMan);
	m_scene.setView(_camera);
	for (Ghost* ghost : m_ghosts)
		m_scene.draw(*ghost);

	m_scene.display();

	window.draw(sf::Sprite(m_scene.getTexture()));

	window.draw(m_scoreText);
	window.draw(m_levelText);
	window.draw(m_remainingDotsText);

	for (unsigned int i = 0; i < _pacMan->getLife(); i++)
		window.draw(m_liveSprite[i]);

}
