#include "GetReadyState.hpp"

GetReadyState* GetReadyState::GetInstance(Game* game, GameState* playingState)
{
	static GetReadyState* _instance = new GetReadyState(game, playingState);
	return _instance;
}

GetReadyState::GetReadyState(Game* game, GameState* playingState)
	:GameState(game)
	, m_playingState(playingState)
{
	_text.setFont(game->getFont());
	_text.setString("Press Start when you are ready...");
	_text.setCharacterSize(15);
	_text.setPosition(50, 220);
}

void GetReadyState::insertCoin()
{
}

void GetReadyState::pressButton()
{
	getGame()->changeGameState(GameState::Playing);
}

void GetReadyState::moveStick(sf::Vector2i direction)
{
}

void GetReadyState::update(sf::Time delta)
{
	m_playingState->update(delta);
}

void GetReadyState::draw(sf::RenderWindow& window)
{
	m_playingState->draw(window);
	window.draw(_text);
}
