#include "Game.hpp"

Game::Game()
:window(sf::VideoMode(480, 500), "PAC MAN")
{
	if (!font.loadFromFile("assets/font.ttf"))
		throw std::runtime_error("Unable to load font file");

	if (!logo.loadFromFile("assets/logo.png"))
		throw std::runtime_error("Unable to load logo file");

	if (!texture.loadFromFile("assets/texture.png"))
		throw std::runtime_error("Unable to load texture file");

	gameStates[GameState::NoCoin] = NoCoinState::GetInstance(this);
	gameStates[GameState::Playing] = PlayingState::GetInstance(this);
	gameStates[GameState::GetReady] = GetReadyState::GetInstance(this, gameStates[GameState::Playing]);
	gameStates[GameState::Won] = WonState::GetInstance(this, gameStates[GameState::Playing]);
	gameStates[GameState::Lost] = LostState::GetInstance(this, gameStates[GameState::Playing]);
	currentState = gameStates[GameState::NoCoin];
}

Game::~Game()
{
	for (GameState* gameState : gameStates)
	{
		delete gameState;
	}
}

void Game::changeGameState(GameState::State state)
{
	currentState = this->gameStates[state];
}

sf::Font& Game::getFont()
{
	return font;
}

sf::Texture& Game::getLogo()
{
	return logo;
}

sf::Texture& Game::getTexture()
{
	return texture;
}

void Game::run()
{
	sf::Clock frameClock;
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed)
			{				
				if (event.key.code == sf::Keyboard::Escape)
					window.close();

				if (event.key.code == sf::Keyboard::Left)
					currentState->moveStick(sf::Vector2i(-1, 0));
				if (event.key.code == sf::Keyboard::Right)
					currentState->moveStick(sf::Vector2i(1, 0));
				if (event.key.code == sf::Keyboard::Up)
					currentState->moveStick(sf::Vector2i(0, -1));
				if (event.key.code == sf::Keyboard::Down)
					currentState->moveStick(sf::Vector2i(0, 1));

				if (event.key.code == sf::Keyboard::Enter) {
					if (currentState == gameStates[GameState::GetReady])
						currentState->pressButton();
					else
						currentState->insertCoin();
				}
			}

		}
		currentState->update(frameClock.restart());
		window.clear();
		currentState->draw(window);
		window.display();
	}
}