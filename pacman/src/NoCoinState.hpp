#ifndef PACMAN_NOCOINSTATE_HPP
#define PACMAN_NOCOINSTATE_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameState.hpp"
#include "Game.hpp"

class NoCoinState: public GameState {

	public:

		NoCoinState(Game* game);
		static NoCoinState* GetInstance(Game* game);
		void insertCoin();
		void pressButton();
		void moveStick(sf::Vector2i direction);
		void update(sf::Time delta);
		void draw(sf::RenderWindow& window);

	private:
		sf::Text _text;
		sf::Sprite _sprite;
		bool _displayText;
};

#endif //PACMAN_NOCOINSTATE_HPP