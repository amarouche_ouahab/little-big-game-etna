#include "Animator.hpp"

Animator::Animator()
: _currentFrame(0)
, _duration(sf::Time::Zero)
, _isPlaying(false)
, _loop(false)
{
}

void Animator::addFrame(sf::IntRect frame)
{
	_frames.push_back(frame);
}

void Animator::play(sf::Time duration, bool loop)
{
	_isPlaying = true;
	_loop = loop;
	_duration = duration;
}

bool Animator::isPlaying() const
{
	return _isPlaying;
}

void Animator::update(sf::Time delta)
{
	if (!_isPlaying)
		return;

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	sf::Time frameDuration = _duration / static_cast<float>(_frames.size());

	while (timeBuffer > frameDuration)
	{
		_currentFrame++;
		if (_currentFrame == _frames.size())
		{
			if (!_loop)
				_isPlaying = false;

			_currentFrame = 0;
		}
		timeBuffer -= frameDuration;
	}
}

void Animator::animate(sf::Sprite& sprite)
{
	sprite.setTextureRect(_frames[_currentFrame]);
}
