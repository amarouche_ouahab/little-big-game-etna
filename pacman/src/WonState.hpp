#ifndef PACMAN_WONSTATE_HPP
#define PACMAN_WONSTATE_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameState.hpp"
#include "PlayingState.hpp"
#include "Game.hpp"

class PlayingState;

class WonState: public GameState {

	public:

		WonState(Game* game, GameState* playingState);
		static WonState* GetInstance(Game* game, GameState* playingState);
		void insertCoin();
		void pressButton();
		void moveStick(sf::Vector2i direction);
		void update(sf::Time delta);
		void draw(sf::RenderWindow& window);

	private:
		sf::Text _text;

		PlayingState* m_playingState;
};

#endif //PACMAN_WONSTATE_HPP