#include "PacMan.hpp"

PacMan::PacMan(sf::Texture& texture)
: _visual(texture)
, _isDead(false)
, _isDying(false)
, _life(3)
{
	setOrigin(20, 20);

	_runAnimator.addFrame(sf::IntRect(0, 32, 40, 40));
	_runAnimator.addFrame(sf::IntRect(0, 72, 40, 40));

	_dieAnimator.addFrame(sf::IntRect(0, 32, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(0, 72, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(0, 112, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(40, 112, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(80, 112, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(120, 112, 40, 40));
	_dieAnimator.addFrame(sf::IntRect(160, 112, 40, 40));

	_runAnimator.play(sf::seconds(0.25), true);
}

void PacMan::die()
{
	if (!_isDying)
	{
		_dieAnimator.play(sf::seconds(1), false);
		_isDying = true;
	}
}

bool PacMan::isDying() const
{
	return _isDying;
}

bool PacMan::isDead() const
{
	return _isDead;
}

void PacMan::reset()
{
	_isDying = false;
	_isDead = false;

	_runAnimator.play(sf::seconds(0.25), true);
	_runAnimator.animate(_visual);
}

int PacMan::getLife() const
{
	return _life;
}

void PacMan::setLife(int life)
{
	_life = life;
}

void PacMan::lostLife()
{
	_life--;
}

void PacMan::update(sf::Time delta)
{
	if (!_isDying && !_isDead)
	{
		_runAnimator.update(delta);
		_runAnimator.animate(_visual);
	} else{
		_dieAnimator.update(delta);
		_dieAnimator.animate(_visual);

		if (!_dieAnimator.isPlaying())
		{
			_isDying = false;
			_isDead = true;
		}
	}
	Character::update(delta);
}

void PacMan::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	if (!_isDead) {
		target.draw(_visual, states);
	}
}
