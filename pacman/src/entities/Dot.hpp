#ifndef PACMAN_DOT_HPP
#define PACMAN_DOT_HPP

#include <SFML/Graphics.hpp>

sf::CircleShape getDot();
sf::CircleShape getSuperDot();

#endif // !PACMAN_DOT_HPP
