#ifndef PACMAN_CHARACTER_HPP
#define PACMAN_CHARACTER_HPP

#include <SFML/Graphics.hpp>
#include "../Maze.hpp"
#include <array>

class Character : public sf::Drawable, public sf::Transformable
{
public:
	Character();
	float getSpeed() const;
	void setSpeed(float speed);
	virtual void update(sf::Time delta);
	void setDirection(sf::Vector2i direction);
	sf::Vector2i getDirection() const;
	void setMaze(Maze* maze);
	bool willMove() const;
	sf::FloatRect getCollisionBox() const;

protected:
	virtual void changeDirection() {};

private:
	float _speed;
	Maze* m_maze;

	sf::Vector2i m_currentDirection;
	sf::Vector2i m_nextDirection;

	sf::Vector2i m_previousIntersection;
	std::array<bool, 4> m_availableDirections;
};

#endif // !PACMAN_CHARACTER_HPP
