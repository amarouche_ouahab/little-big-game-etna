#ifndef PACMAN_GHOST_HPP
#define PACMAN_GHOST_HPP

#include "Character.hpp"
#include "Pacman.hpp"
#include "../Animator.hpp"

class Ghost: public Character
{
public:
	enum State
	{
		Strong,
		Weak
	};
	Ghost(sf::Texture& texture, PacMan* pacMan);
	void setWeak(sf::Time duration);
	bool isWeak() const;
	void update(sf::Time delta);

protected:
	void changeDirection() override;

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	bool _isWeak;
	sf::Time _weaknessDuration;
	sf::Sprite _visual;
	Animator _strongAnimator;
	Animator _weakAnimator;

	PacMan* _pacMan;
};

#endif // !PACMAN_GHOST_HPP
