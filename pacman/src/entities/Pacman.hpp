#ifndef PACMAN_PACMAN_HPP
#define PACMAN_PACMAN_HPP

#include "Character.hpp"
#include "../Animator.hpp"

class PacMan : public Character
{
public:
	PacMan(sf::Texture& texture);
	void die();
	bool isDying() const;
	bool isDead() const;
	void update(sf::Time delta);
	void reset();
	int getLife() const;
	void setLife(int life);
	void lostLife();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Sprite _visual;
	bool _isDead;
	bool _isDying;
	int _life;
	Animator _runAnimator;
	Animator _dieAnimator;
};
#endif // !PACMAN_PACMAN_HPP
