#ifndef PACMAN_BONUS_HPP
#define PACMAN_BONUS_HPP

#include <SFML/Graphics.hpp>

class Bonus: public sf::Drawable, public sf::Transformable
{
public:

	enum Fruit
	{
		Banana,
		Apple,
		Cherry
	};

	Bonus(sf::Texture& texture);

private:
	sf::Sprite _visual;
	void setFruit(Fruit fruit);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif // !PACMAN_BONUS_HPP
