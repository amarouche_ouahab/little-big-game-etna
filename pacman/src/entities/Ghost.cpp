#include "Ghost.hpp"

Ghost::Ghost(sf::Texture& texture, PacMan* pacMan)
	:_visual(texture),
	_isWeak(false),
	_weaknessDuration(sf::Time::Zero),
	_pacMan(pacMan)
{
	setOrigin(20, 20);

	//_strongAnimator.addFrame(sf::IntRect(40, 32, 40, 40));
	_strongAnimator.addFrame(sf::IntRect(80, 32, 40, 40));

	_weakAnimator.addFrame(sf::IntRect(40, 72, 40, 40));
	//_weakAnimator.addFrame(sf::IntRect(80, 72, 40, 40));

	_strongAnimator.play(sf::seconds(0.25), true);
	_weakAnimator.play(sf::seconds(1), true);
}

void Ghost::setWeak(sf::Time duration)
{
	_weaknessDuration = duration;
	_isWeak = true;
}

bool Ghost::isWeak() const
{
	return _isWeak;
}

void Ghost::update(sf::Time delta)
{
	if (_isWeak)
	{
		_weaknessDuration -= delta;

		if (_weaknessDuration <= sf::Time::Zero)
		{
			_isWeak = false;
			_strongAnimator.play(sf::seconds(0.25), true);
		}
	}

	if (!_isWeak)
	{
		_strongAnimator.update(delta);
		_strongAnimator.animate(_visual);
	}
	else
	{
		_weakAnimator.update(delta);
		_weakAnimator.animate(_visual);
	}
	Character::update(delta);
}

void Ghost::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(_visual, states);
}


void Ghost::changeDirection()
{
	static sf::Vector2i directions[4] = {
		sf::Vector2i(1, 0),
		sf::Vector2i(0, 1),
		sf::Vector2i(-1, 0),
		sf::Vector2i(0, -1)
	};

	std::map<float, sf::Vector2i> directionProb;

	float targetAngle;

	sf::Vector2f distance = _pacMan->getPosition() - getPosition();

	targetAngle = std::atan2(distance.x, distance.y) * (180 / 3.14);

	for (auto direction : directions)
	{
		float directionAngle = std::atan2(direction.x, direction.y) * (180 / 3.14);

		//Normalize the angle difference
		float diff = 180 - std::abs(std::abs(directionAngle - targetAngle) - 180);

		directionProb[diff] = direction;
	}
	setDirection(directionProb.begin()->second);

	auto it = directionProb.begin();

	do
	{
		setDirection(it->second);
		it++;
	} while (!willMove());
}