#ifndef PACMAN_GAMESTATE_HPP
#define PACMAN_GAMESTATE_HPP

#include <SFML/Graphics.hpp>
// #include "Game.hpp"

class Game;

class GameState {

public:
	enum State {
		NoCoin,
		GetReady,
		Playing,
		Won,
		Lost,
		Count
	};

	GameState(Game* game);
	virtual void insertCoin() = 0;
	virtual void pressButton() = 0;
	virtual void moveStick(sf::Vector2i direction) = 0;
	virtual void update(sf::Time delta) = 0;
	virtual void draw(sf::RenderWindow &window) = 0;
	Game* getGame() const;

private:
	Game* game;
};

#endif //PACMAN_GAMESTATE_HPP