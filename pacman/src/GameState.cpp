#include "GameState.hpp"

GameState::GameState(Game* game)
	:game(game)
{

}

Game* GameState::getGame() const
{
	return game;
}