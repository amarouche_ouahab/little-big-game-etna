#ifndef PACMAN_GETREADYSTATE_HPP
#define PACMAN_GETREADYSTATE_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Game.hpp"

class GetReadyState: public GameState {

	public:

		GetReadyState(Game* game, GameState* playingState);
		static GetReadyState* GetInstance(Game* game, GameState* playingState);
		void insertCoin();
		void pressButton();
		void moveStick(sf::Vector2i direction);
		void update(sf::Time delta);
		void draw(sf::RenderWindow& window);

	private:
		sf::Text _text;
		GameState* m_playingState;
};

#endif //PACMAN_GETREADYSTATE_HPP