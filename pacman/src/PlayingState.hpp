#ifndef PACMAN_PLAYING_HPP
#define PACMAN_PLAYING_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameState.hpp"
#include "Game.hpp"
#include "maze.hpp"
#include "entities/PacMan.hpp"
#include "entities/Ghost.hpp"

class PlayingState: public GameState {

	public:

		PlayingState(Game* game);
		~PlayingState();
		static PlayingState* GetInstance(Game* game);
		void insertCoin();
		void pressButton();
		void moveStick(sf::Vector2i direction);
		void update(sf::Time delta);
		void draw(sf::RenderWindow& window);
		void loadNextLevel();
		void resetToZero();
		void resetCurrentLevel();
		void resetLiveCount();
		void moveCharactersToInitialPosition();
		void updateCameraPosition();

	private:
		sf::Text _text;
		PacMan* _pacMan;
		std::vector<Ghost*> m_ghosts;
		Maze _maze;
		sf::View _camera;
		sf::RenderTexture m_scene;

		sf::Text m_scoreText;
		sf::Text m_levelText;
		sf::Text m_remainingDotsText;
		sf::Sprite m_liveSprite[3];

		int m_level;
		int m_liveCount;
		int m_score;
};

#endif //PACMAN_PLAYING_HPP