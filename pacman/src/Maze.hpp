#ifndef PACMAN_MAZE_HPP
#define PACMAN_MAZE_HPP

#include <vector>
#include <SFML/Graphics.hpp>
#include "entities/Dot.hpp"
#include <iostream>

class Maze: public sf::Drawable
{
public:
	Maze(sf::Texture& texture);
	void loadLevel(std::string name);
	sf::Vector2i getPacManPosition() const;
	std::vector<sf::Vector2i> getGhostPositions() const;
	std::size_t positionToIndex(sf::Vector2i position) const;
	sf::Vector2i indexToPosition(std::size_t index) const;
	sf::Vector2i mapPixelToCell(sf::Vector2f pixel) const;
	sf::Vector2f mapCellToPixel(sf::Vector2i cell) const;
	sf::Vector2i getSize() const;
	bool isWall(sf::Vector2i position) const;
	bool isDot(sf::Vector2i position) const;
	bool isSuperDot(sf::Vector2i position) const;
	bool isBonus(sf::Vector2i position) const;
	int getRemainingDots() const;
	void pickObject(sf::Vector2i position);

private:
	enum CellData
	{
		Empty,
		Wall,
		Dot,
		SuperDot,
		Bonus
	};

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Vector2i m_mazeSize;
	std::vector<CellData> m_mazeData;
	sf::Vector2i m_pacManPosition;
	std::vector<sf::Vector2i> m_ghostPositions;
	sf::RenderTexture m_renderTexture;
	sf::Texture& m_texture;

};
#endif // !PACMAN_MAZE_HPP
