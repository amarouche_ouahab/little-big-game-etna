#ifndef PACMAN_ANIMATOR_HPP
#define PACMAN_ANIMATOR_HPP

#include <SFML/Graphics.hpp>
#include <iostream>

class Animator
{
public:
	Animator();
	void addFrame(sf::IntRect frame);
	void play(sf::Time duration, bool loop);
	bool isPlaying() const;
	void update(sf::Time delta);
	void animate(sf::Sprite& sprite);

private:
	std::vector<sf::IntRect> _frames;
	bool _loop;
	bool _isPlaying;
	sf::Time _duration;
	unsigned int _currentFrame;
};

#endif // !PACMAN_ANIMATOR_HPP
