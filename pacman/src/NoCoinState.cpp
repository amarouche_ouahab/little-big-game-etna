#include "NoCoinState.hpp"


NoCoinState* NoCoinState::GetInstance(Game* game)
{
	static NoCoinState* _instance = new NoCoinState(game);
	return _instance;
}

NoCoinState::NoCoinState(Game* game)
:GameState(game)
{
	_displayText = true;
	_sprite.setTexture(game->getLogo());
	_sprite.setPosition(100, 90);

	_text.setFont(game->getFont());
	_text.setString("Insert coin");
	_text.setPosition(100, 240);
}

void NoCoinState::insertCoin()
{
	getGame()->changeGameState(GameState::GetReady);
	std::cout << "insert" << std::endl;
}

void NoCoinState::pressButton()
{
	std::cout << "press" << std::endl;
}

void NoCoinState::moveStick(sf::Vector2i direction)
{
	std::cout << "move" << std::endl;
}

void NoCoinState::update(sf::Time delta)
{
	// sf::Time timeBuffer = sf::Time::Zero;
	// timeBuffer += delta;

	//while (timeBuffer >= sf::seconds(0.5)) {
	//	_displayText = !_displayText;
	//	timeBuffer -= sf::seconds(1);
	//}
}

void NoCoinState::draw(sf::RenderWindow& window)
{
	window.draw(_sprite);

//	if (_displayText)
		window.draw(_text);

}
